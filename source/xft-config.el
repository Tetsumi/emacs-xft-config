;;; xft-config.el --- Xft configuration helper

;; Copyright (C) 2019 Tetusmi

;; Author: Tetsumi <tetsumi@protonmail.com>
;; URL: 
;; Package-Requires: ((emacs "26"))
;; Version: 1.0

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;;; Code:

(require 'xft-config-core)

(defun xft-config-set-rendering (antialias
				 hinting
				 autohint
				 hintstyle			   
				 rgba
				 lcdfilter)
  (interactive
   (cl-labels ((cpread (pr choices default)
		       (completing-read (format "%s (default %s): " pr default)
					choices
					nil nil nil nil default))
	       (cpreadtf (pr default)
			 (cpread pr '("true" "false") default)))
     (list (cpreadtf "antialias" "true")
	   (cpreadtf "hinting" "true")
	   (cpreadtf "autohint" "false")
	   (cpread "hintstyle"
		   '("hintnone" "hintslight" "hintmedium" "hintfull")
		   "hintslight")
	   (cpread "rgba"
		   '("none" "rgb" "bgr" "v-rgb" "v-bgr")
		   "rgb")
	   (cpread "lcdfilter"
		   '("lcdnone" "lcddefault" "lcdlight" "lcdlegacy")
		   "lcddefault"))))
  
    (xft-config-set-pattern (concat ":antialias=" antialias
				    ":hinting=" hinting
				    ":autohint=" autohint
				    ":hintstyle=" hintstyle
				    ":rgba=" rgba
				    ":lcdfilter=" lcdfilter))
    (font-setting-change-default-font (frame-parameter nil 'display) nil))

(provide 'xft-config)
;;; xft-config.el ends here
