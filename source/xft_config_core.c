/*
  xft-config-core
 
  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2019 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include <X11/Xft/Xft.h>
#include <gdk/gdkx.h>
#include <emacs-module.h>
#include <string.h>
#include <stdlib.h>
     
int plugin_is_GPL_compatible;

#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_REVISION 0
#define VERSION_STRING "1.0.0"

static emacs_value signalError
(emacs_env  *env,
 const char *errorName,
 const char *errorDesc)
{
	emacs_value desc = NULL;
	
	if (errorDesc)
		desc = env->make_string(env, errorDesc, strlen(errorDesc));
	
	if (NULL == desc)
		desc = env->intern(env, "nil");
	
        env->non_local_exit_signal(env,
				   env->intern(env, errorName),
				   desc);
	return NULL;
}

static Display *getDefaultDisplay
/*
 * Retrieve the default X display through GDK.
 * Signal an error to emacs if fail.
 */
(emacs_env *env)
{
	Display *dpy = gdk_x11_display_get_xdisplay(gdk_display_get_default());
	
	if (!dpy)
		signalError(env, "error", "Can't open X display.");
	
	return dpy;
}

static FcPattern *createFcPattern
/*
 * Create an empty FcPattern.
 * Signal an error to emacs if fail.
 */
(emacs_env *env)
{
	FcPattern *pat = FcPatternCreate();

	if (!pat)
		signalError(env, "error", "Can't parse FC Pattern.");
	
	return pat;
}


static emacs_value XFT_CONFIG_get_pattern
(emacs_env   *env,
  long int    nargs,
 emacs_value  args[],
 void        *data)
{
	Display *dpy = getDefaultDisplay(env);

	if (!dpy)
		return NULL;

	int screenIndex = XDefaultScreen(dpy);
	FcPattern *pat = createFcPattern(env);

	if (!pat)
		return NULL;
	
	XftDefaultSubstitute(dpy, screenIndex, pat);
	
	FcChar8 *ps = FcNameUnparse(pat);

	if (!ps)
	{
		signalError(env, "error", "Can't unparse FC Pattern.");
		FcPatternDestroy(pat);
		return NULL;
	}

	emacs_value eStr = env->make_string(env, ps, strlen(ps));
	
	free(ps);
	FcPatternDestroy(pat);
		
	return eStr;
}

static emacs_value XFT_CONFIG_set_pattern
(emacs_env   *env,
  long int    nargs,
 emacs_value  args[],
 void        *data)
{
	ptrdiff_t size = 0;
	FcChar8 *buf = NULL;

	env->copy_string_contents(env, args[0], buf, &size);
	
	if (env->non_local_exit_check(env) != emacs_funcall_exit_return)
		return NULL;
	
	buf = malloc(size);
	
	if (!buf)
	{
		signalError(env, "memory-full", NULL);
		return NULL;
	}
	
	env->copy_string_contents(env, args[0], buf, &size);


	Display *dpy = getDefaultDisplay(env);

	if (!dpy)
	{
		free(buf);
		return NULL;
	}
	
	int screenIndex = XDefaultScreen(dpy);	
	FcPattern *pat = FcNameParse(buf);

	if (!pat)
	{
		free(buf);
		signalError(env, "error", "Can't parse FC Pattern.");
		return NULL;
	}

	XftDefaultSubstitute(dpy, screenIndex, pat);
	
	if (!XftDefaultSet(dpy, pat))
	{
		free(pat);
		signalError(env, "error", "Can't set Xft default.");
	}
	
	free(buf);
	
        return env->intern(env, "nil");
}

static emacs_value createEmacsFunction
(emacs_env   *env,
 emacs_value (*function) (emacs_env *env,
			  ptrdiff_t nargs,
			  emacs_value args[],
			  void *),
 const char* name,
 const char* doc,
 ptrdiff_t min_arity,
 ptrdiff_t max_arity)
{
	emacs_value fun = env->make_function(
		env,
		min_arity,
		max_arity,
		function,
		doc,
		NULL);

	if (env->non_local_exit_check(env) != emacs_funcall_exit_return)
		return NULL;
		
	env->funcall(env,
		     env->intern(env, "defalias"),
		     2,
		     (emacs_value[]){
                     	  env->intern(env, name),
		          fun
		     });

	if (env->non_local_exit_check(env) != emacs_funcall_exit_return)
		return NULL;

	return fun;
}

int emacs_module_init
(struct emacs_runtime *ert)
{
	if (ert->size < sizeof(*ert))
		return 1;
	
	emacs_env *env = ert->get_environment(ert);
	
	if (env->size < sizeof(*env))
                   return 2;

	if (!createEmacsFunction(env,
				 XFT_CONFIG_get_pattern,
				 "xft-config-get-pattern",
				 "Get Xft's default pattern.",
				 0,
				 0))
		return 2;

	if (!createEmacsFunction(env,
				 XFT_CONFIG_set_pattern,
				 "xft-config-set-pattern",
				 "Set Xft's default pattern.",
				 1,
				 1))
		return 2;

	
	env->funcall(env,
		     env->intern (env, "provide"),
		     1,
		     (emacs_value[]){env->intern(env, "xft-config-core")}
		);
	return 0;
}
