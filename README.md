xft-config
================

An Emacs package to configure Xft font rendering.  This package includes a  
native dynamic module.

Demo
----------------

![Demo video (webm)](https://webm.red/3Mr9.webm)

How to use
================

 Name    | Description       
---------|------------------|
`(xft-config-get-pattern)` | Return the current Xft's default pattern.
`(xft-config-set-pattern fcpattern)` | Set fcpattern as Xft's default pattern.
`(xft-config-set-rendering antialias hinting autohint hintstyle rgba lcdfilter)` | Update Xft's default pattern then refresh Emacs's fonts cache. This is also an interactive command. 

Examples
----------------

```
(xft-config-get-pattern)  
-> "-12:familylang=en,en-us:stylelang=en,en-us:fullnamelang=en,en-us:slant=0..."

(xft-config-set-pattern ":antialias=false:hinting=false:lcdfilter=lcdnone")
-> nil

;; Need to refresh Emacs's fonts cache to see the effects of the new pattern.
(set-frame-font "Ubuntu Mono-12" t nil)
-> nil

;; Automatically refresh the fonts cache.
(xft-config-set-rendering "true" "true" "true" "hintlight" "rgb" "lcddefault")
-> nil
```

How to build
================

Execute `make.bash`

Dependencies
----------------

* Xlib
* Xft
* Freetype
* GDK

How to install
================

After building, copy the following files to your emacs packages folder:

```
./bin/xft-config-core.so
./source/xft-config.el
```

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)
