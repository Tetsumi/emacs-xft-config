#!/bin/bash

nd='-DNDEBUG'

if [ "${1+defined}" ] && [ $1 = "-d" ]; then
    nd="-g"
fi

gcc -O3 \
    ${nd} \
    $(pkg-config --cflags --libs freetype2 gdk-3.0) \
    -shared \
    -fPIC \
    -o ./bin/xft-config-core.so \
    ./source/*.c
